import QtQuick 2.4
import QtQuick.Window 2.2

Window {
    visible: true
    Rectangle {
        anchors.fill: parent
        color: "red"

        Text {
            anchors.centerIn: parent
            text: Hook.currentVal ? "Angkat" : "Tutup"
            color: "white"
            font.pointSize: 50
        }
    }
}
