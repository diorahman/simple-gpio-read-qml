#include "hook.h"
#include <QFile>
#include <QDebug>
#include <QTimer>
#include <unistd.h>
#include <fcntl.h>

Hook::Hook(QObject *parent) : QObject(parent), m_currentVal(-1)
{
    QFile file("/sys/class/gpio/export");
    if (!file.open(QIODevice::WriteOnly)) {
        qDebug() << "GPIO export failed.";
        return;
    }
    file.write("9");
    file.close();

    m_timer = new QTimer(this);
    connect(m_timer, &QTimer::timeout, this, &Hook::read);
}

void Hook::start()
{
    read();
    m_timer->start(1000);
}

void Hook::read()
{
    QFile pe11("/sys/class/gpio/gpio9_pe11/value");
    if (!pe11.open(QIODevice::ReadOnly)) {
        qDebug() << "GPIO read failed.";
        return;
    }
    ::fcntl(pe11.handle(), O_NONBLOCK);
    QByteArray raw = pe11.readAll();

    if (!raw.isEmpty()) {
        int currentVal = raw.trimmed().toInt();
        if (currentVal != m_currentVal) {
            qDebug() << "gpio read: " << currentVal;
            m_currentVal = currentVal;
            emit currentValChanged();
        }
    }
}
