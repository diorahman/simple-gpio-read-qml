TEMPLATE = app

QT += qml quick

SOURCES += main.cpp \
    hook.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
# include(deployment.pri)
target.path = /home/diorahman/apps
INSTALLS += target

HEADERS += \
    hook.h
