#ifndef HOOK_H
#define HOOK_H

#include <QObject>

class QTimer;

class Hook : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int currentVal READ currentVal NOTIFY currentValChanged)

public:
    explicit Hook(QObject *parent = 0);

    Q_INVOKABLE void start();
    int currentVal() {
        return m_currentVal;
    }

signals:
    void currentValChanged();

private:
    QTimer *m_timer;
    void read();
    int m_currentVal;
};

#endif // HOOK_H
