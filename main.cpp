#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "hook.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    Hook hook;
    engine.rootContext()->setContextProperty("Hook", &hook);
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    hook.start();
    return app.exec();
}
